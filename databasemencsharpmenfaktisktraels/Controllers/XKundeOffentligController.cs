﻿using databasemencsharpmenfaktisktraels.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace databasemencsharpmenfaktisktraels.Controllers
{
    public class XKundeOffentligController : Controller
    {
        KundeKartotekEntities DB = new KundeKartotekEntities();
        // GET: XKundeOffentlig
        public ActionResult Index()
        {
            List<XKundeOffentlig> Kundeliste = DB.XKundeOffentlig.ToList();
            return View(Kundeliste);
        }

        // GET: XKundeOffentlig/Details/5
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return HttpNotFound();

            }
            XKundeOffentlig kunde = DB.XKundeOffentlig.Where(w => w.KundeID == id).Single();
            if(kunde == null)
            {
                return HttpNotFound();
            }
            return View(kunde);
        }

        // GET: XKundeOffentlig/Create
        public ActionResult Create()
        {
            ViewBag.KundeId = new SelectList(DB.XKunde, "KundeID", "KundeID");
            return View();
        }

        // POST: XKundeOffentlig/Create
        [HttpPost]
        public ActionResult Create(XKundeOffentlig kundeOffentlig)
        {
            if (ModelState.IsValid)
            {
                DB.XKundeOffentlig.Add(kundeOffentlig);
                DB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
            ViewBag.KundeId = new SelectList(DB.XKunde, "KundeID", "KundeID");
            return View(kundeOffentlig);
            }

        }

        // GET: XKundeOffentlig/Edit/5
        public ActionResult Edit(int id)
        {
            return View(DB.XKundeOffentlig.Find(id));
        }

        // POST: XKundeOffentlig/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, XKundeOffentlig kundeOffentlig)
        {
            if (ModelState.IsValid)
            {
                DB.Entry(kundeOffentlig).State = EntityState.Modified;
                DB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return View(kundeOffentlig);
            }
        }

        // GET: XKundeOffentlig/Delete/5
        public ActionResult Delete(int id)
        {
            return View(DB.XKundeOffentlig.Find(id));
        }

        // POST: XKundeOffentlig/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, XKundeOffentlig kundeOffentlig)
        {
            if (ModelState.IsValid)
            {
                XKundeOffentlig kunde = DB.XKundeOffentlig.Find(id);
                DB.XKundeOffentlig.Remove(kunde);
                DB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return View(kundeOffentlig);
            }

        }
    }
}
