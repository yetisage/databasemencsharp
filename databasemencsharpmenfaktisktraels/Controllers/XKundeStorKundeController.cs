﻿using databasemencsharpmenfaktisktraels.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace databasemencsharpmenfaktisktraels.Controllers
{
    public class XKundeStorKundeController : Controller
    {
        KundeKartotekEntities DB = new KundeKartotekEntities();
        // GET: XKundeStorKunde
        public ActionResult Index()
        {
            List<XKundeStorKunde> Kundeliste = DB.XKundeStorKunde.ToList();
            return View(Kundeliste);
        }

        // GET: XKundeStorKunde/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            XKundeStorKunde kunde = DB.XKundeStorKunde.Where(w => w.KundeID == id).Single();
            if (kunde == null)
            {
                return HttpNotFound();
            }
            return View(kunde);
        }

        // GET: XKundeStorKunde/Create
        public ActionResult Create()
        {
            ViewBag.KundeID = new SelectList(DB.XKunde, "KundeID", "KundeID");
            return View();
        }

        // POST: XKundeStorKunde/Create
        [HttpPost]
        public ActionResult Create(XKundeStorKunde kundeStorKunde)
        {
            if (ModelState.IsValid)
            {
                DB.XKundeStorKunde.Add(kundeStorKunde);
                DB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.KundeID = new SelectList(DB.XKunde, "KundeID", "KundeID");
                return View(kundeStorKunde);
            }
        }

        // GET: XKundeStorKunde/Edit/5
        public ActionResult Edit(int id)
        {
            return View(DB.XKundeStorKunde.Find(id));
        }

        // POST: XKundeStorKunde/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, XKundeStorKunde kundeStorKunde)
        {
            if (ModelState.IsValid)
            {
                DB.Entry(kundeStorKunde).State = EntityState.Modified;
                DB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return View(kundeStorKunde);
            }
        }

        // GET: XKundeStorKunde/Delete/5
        public ActionResult Delete(int id)
        {
            return View(DB.XKundeStorKunde.Find(id));
        }

        // POST: XKundeStorKunde/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, XKundeStorKunde kundeStorKunde)
        {
            if (ModelState.IsValid)
            {
            XKundeStorKunde kunde = DB.XKundeStorKunde.Find(id);
            DB.XKundeStorKunde.Remove(kunde);
            DB.SaveChanges();

            return RedirectToAction("Index");
            }
            else
            {
                return View(kundeStorKunde);
            }

        }
    }
}
