﻿using databasemencsharpmenfaktisktraels.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace databasemencsharpmenfaktisktraels.Controllers
{
    public class XKundePrivatController : Controller
    {
        KundeKartotekEntities DB = new KundeKartotekEntities();
        // GET: XKundePrivat
        public ActionResult Index()
        {
            List<XKundePrivat> Kundeliste = DB.XKundePrivat.ToList();
            return View(Kundeliste);
        }

        // GET: XKundePrivat/Details/5
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return HttpNotFound();

            }
            XKundePrivat kunde = DB.XKundePrivat.Where(w => w.KundeID == id).Single();
            if(kunde == null)
            {
                return HttpNotFound();
            }
            return View(kunde);
        }

        // GET: XKundePrivat/Create
        public ActionResult Create()
        {
            ViewBag.KundeID = new SelectList(DB.XKunde, "KundeID", "KundeID");
            return View();
        }

        // POST: XKundePrivat/Create
        [HttpPost]
        public ActionResult Create(XKundePrivat kundePrivat)
        {
            if (ModelState.IsValid)
            {
                DB.XKundePrivat.Add(kundePrivat);
                DB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.KundeID = new SelectList(DB.XKunde, "KundeID", "Fornavn");
                return View(kundePrivat);
            }
        }

        // GET: XKundePrivat/Edit/5
        public ActionResult Edit(int id)
        {
            return View(DB.XKundePrivat.Find(id));
        }

        // POST: XKundePrivat/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, XKundePrivat kundePrivat)
        {
            if (ModelState.IsValid)
            {
                DB.Entry(kundePrivat).State = EntityState.Modified;
                DB.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View(kundePrivat);
            }
        }

        // GET: XKundePrivat/Delete/5
        public ActionResult Delete(int id)
        {
            return View(DB.XKundePrivat.Find(id));
        }

        // POST: XKundePrivat/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, XKundePrivat kundePrivat)
        {
            if (ModelState.IsValid)
            {
                XKundePrivat kunde = DB.XKundePrivat.Find(id);
                DB.XKundePrivat.Remove(kunde);
                DB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return View(kundePrivat);
            }
        }
    }
}
