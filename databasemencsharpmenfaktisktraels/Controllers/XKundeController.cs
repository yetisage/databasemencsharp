﻿using databasemencsharpmenfaktisktraels.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace databasemencsharpmenfaktisktraels.Controllers
{
    public class XKundeController : Controller
    {
            KundeKartotekEntities DB = new KundeKartotekEntities();
        // GET: XKunde
        public ActionResult Index()
        {
            List<XKunde> Kundeliste = DB.XKunde.ToList();
            return View(Kundeliste);
        }

        // GET: XKunde/Details/5
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return HttpNotFound();

            }
            XKunde kunde = DB.XKunde.Where(w => w.KundeID == id).Single();
            if(kunde == null)
            {
                return HttpNotFound();
            }
            return View(kunde);
        }

        // GET: XKunde/Create
        public ActionResult Create()
        {
            XKunde kunde = new XKunde();
            kunde.KundeID = DB.XKunde.Max(u => u.KundeID) + 1;
            return View();
        }

        // POST: XKunde/Create
        [HttpPost]
        public ActionResult Create(XKunde kunde)
        {
            kunde.KundeID = DB.XKunde.Max(u => u.KundeID) + 1;
            if (ModelState.IsValid)
            {
                DB.XKunde.Add(kunde);
                DB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return View(kunde);
            }

        }

        // GET: XKunde/Edit/5
        public ActionResult Edit(int id)
        {
            return View(DB.XKunde.Find(id));
        }

        // POST: XKunde/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, XKunde kunde)
        {
            if (ModelState.IsValid)
            {
                DB.Entry(kunde).State = EntityState.Modified;
                DB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return View(kunde);
            }
        }

        // GET: XKunde/Delete/5
        public ActionResult Delete(int id)
        {
            return View(DB.XKunde.Find(id));
        }

        // POST: XKunde/Delete/5
        //change for git
        [HttpPost]
        public ActionResult Delete(int id, XKunde Xkunde)
        {
            if (ModelState.IsValid)
            {
            XKunde kunde = DB.XKunde.Find(id);
            DB.XKunde.Remove(kunde);
            DB.SaveChanges();

            return RedirectToAction("Index");
            }
            else
            {
                return View(Xkunde);
            }
        }
    }
}
